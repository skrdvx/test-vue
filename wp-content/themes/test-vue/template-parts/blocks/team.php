<?php

/**
 * Test Block Template.
 */

// id
$id = $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// class
$className = 'team';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}

// fields
$team = get_field('team');

?>
<section class="<?php echo esc_attr($className); ?>" id="<?php echo esc_attr($id); ?>">
    <?php if( $team ): ?>
        <?php foreach ($team as $item): ?>
            <?php
                $title = get_the_title($item);
                $url = get_post_permalink($item);
                $image = get_field('image', $item);
                $description = get_field('description', $item);
                $email = get_field('email', $item);
            ?>
            <div class="team__item">
                <img class="team__image" src="<?php echo $image; ?>" alt="<?php echo $title; ?>">
                <h3 class="team__title"><?php echo $title; ?></h3>
                <span class="team__description"><?php echo $description; ?></span>
                <a class="team__link" href="<?php echo $url; ?>"><?php echo __( 'на страницу', 'test' ); ?></a>
            </div>
        <?php endforeach; ?>
    <?php endif; ?>
</section>