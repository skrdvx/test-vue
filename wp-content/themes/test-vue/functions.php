<?php
/**
 * Theme functions
 */

// Include PHP dependencies.
require_once __DIR__ . '/vendor/autoload.php';

// Enque scripts and styles.
function test_scripts() {
  wp_register_style('test-css', get_stylesheet_directory_uri() . '/dist/app.css', [], '');
  wp_register_script('test-js', get_stylesheet_directory_uri() . '/dist/app.js', [], '', false);

  wp_enqueue_style('test-css');
  wp_enqueue_script('test-js');
  wp_enqueue_script( 'wp-api' );
}
add_action('wp_enqueue_scripts', 'test_scripts');

// Register Test block.
acf_register_block_type(array(
  'name'              => 'test',
  'title'             => __('Test'),
  'description'       => __('Test block.'),
  'category'          => 'formatting',
  'keywords'          => array( 'test' ),
  'icon'              => 'admin-comments',
  'render_template'   => plugin_dir_path( __FILE__ ) . 'template-parts/blocks/test.php',
  'enqueue_style'     => get_template_directory_uri() . '/assets/scss/blocks/test.scss',
));

// Register ACF field group.
ACFComposer\ACFComposer::registerFieldGroup([
  'name' => 'test',
  'title' => 'Test',
  'style' => 'seamless',
  'fields' => [
    [
      'label' => 'Field',
      'name' => 'field',
      'type' => 'text',
      'aria-label' => '',
      'instructions' => '',
      'required' => 0,
    ],
  ],
  'location' => [
    [
      [
        'param' => 'post_type',
        'operator' => '==',
        'value' => 'post'
      ],
    ],
  ],
  'show_in_rest' => 1,
]);