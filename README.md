## Setup
To start Docker from project root folder:
```
docker-compose up -d
```

To install dependencies from theme folder:

```
composer install
npm install
```