<?php
/**
 * Header template.
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>" />
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
  <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
  <title><?php the_title(); ?> | <?php echo get_bloginfo( 'name' ); ?></title>
  <?php wp_head() ?>
</head>
<body <?php body_class(); ?>>
  <?php get_template_part( 'template-parts/site/header' ); ?>
  <main class="main">