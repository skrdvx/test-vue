<?php

/**
 * Test Block Template.
 */

// id
$id = $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// class
$className = 'person';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}

// fields
$post_id = get_the_ID();
$image = get_field('image', $post_id);
$description = get_field('description', $post_id);
$email = get_field('email', $post_id);
$blocks = get_field('blocks', $post_id);
$accordion = get_field('accordion', $post_id);

?>
<section class="<?php echo esc_attr($className); ?>" id="<?php echo esc_attr($id); ?>">
    <div class="person__column">
        <img class="person__image" src="<?php echo $image; ?>" alt="<?php the_title(); ?>">
        <h3 class="person__description"><?php echo $description; ?></h3>
        <a class="person__email" href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a>
    </div>
    <div class="person__column">
        <?php if( $blocks ): ?>
            <div class="person__blocks">
                <?php foreach ($blocks as $block): ?>
                    <div class="person__block">
                        <h2 class="person__block-title"><?php echo $block['title']; ?></h2>
                        <p class="person__block-text"><?php echo $block['text']; ?></p>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
        <?php if( $accordion ): ?>
            <div class="person__accordion">
                <?php foreach ($accordion as $item): ?>
                    <div class="person__item" data-item="item">
                        <button class="person__item-header" data-item="header">
                            <div class="person__item-title">
                                <span class="person__item-title-close"><?php echo $item['title_close']; ?></span>
                                <span class="person__item-title-open"><?php echo $item['title_open']; ?></span>
                            </div>
                            <div class="person__item-icon">
                                <?php include get_stylesheet_directory() . '/assets/img/icon-plus.svg'; ?>
                                <?php include get_stylesheet_directory() . '/assets/img/icon-minus.svg'; ?>
                            </div>
                        </button>
                        <div class="person__item-text">
                            <?php echo $item['text']; ?>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    </div>
</section>