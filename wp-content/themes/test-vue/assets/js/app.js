import '../scss/app.scss';

import Vue from 'vue';
import TestForm from './components/TestForm.vue';

function onReady() {
  const app = new Vue({
    el: '#test',
    components: {
      'test-form': TestForm,
    },
  });
}

document.addEventListener('DOMContentLoaded', onReady);