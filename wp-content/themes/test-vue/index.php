<?php
/**
 * The main template file
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 */

get_header();

if ( have_posts() ) {

	// Load posts loop.
	while ( have_posts() ) {
		the_post();
	}

} else {

	// If no content, include the "No posts found" template.
	get_template_part( 'template-parts/page-404' );

}

get_footer();
